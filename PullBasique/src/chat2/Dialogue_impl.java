package chat2;

import chat2.Server.Message;
import dialogue.DialoguePOA;

public class Dialogue_impl extends DialoguePOA{

	@Override
	public void connect(String pseudo) {
		Server.getServer().addClient(pseudo);
		System.out.println(pseudo+" connecté");
	}

	@Override
	public void disconnect(String pseudo) {
		Server.getServer().removeClient(pseudo);
		System.out.println(pseudo+" déconnecté");
	}

	@Override
	public String[] getClient() {
		return Server.getServer().getlistClients();
	}

	@Override
	public void sendMessage(String from, String to, String message) {
		Server.getServer().sendMessage(from, to, message);
	}

	@Override
	public String[] getMessage(String pseudo) {
		int nbMessage = Server.getServer().getMessage(pseudo).size();
		String [] message=new String [nbMessage];
		int i=0;
		for(Message m:Server.getServer().getMessage(pseudo)){
			message[i++]=m.messageContent;
		}
		return message;
	}

	@Override
	public boolean _is_a(String repositoryIdentifier) {
		
		return false;
	}


}
