package chat2;

import java.util.ArrayList;
import java.util.Iterator;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;

import dialogue.Dialogue;
import dialogue.DialogueHelper;

public class Server {
	ArrayList<String> listeClients = new ArrayList<String>();
	ArrayList<Message> messagesListe = new ArrayList<Message>();
	static Server server;
	
	public static void main(String[] args) {
		java.util.Properties props = System.getProperties();
		int status = 0;
		ORB orb = null;
		try
		{
			orb = ORB.init(args, props);
			run(orb);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			status = 1;
		}
		if(orb != null)
		{
			try
			{
				orb.destroy();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				status = 1;
			}
		}
		System.exit(status);
	}
	static Server getServer(){
		if(server==null){
			server = new Server();
		}
		return server;
	}
	
	public String[] getlistClients(){
		int size = listeClients.size();
		return (String[])listeClients.toArray(new String[size]);
	}
	public void addClient(String nameClient){
		listeClients.add(nameClient);
		System.out.println("bienvenue dans le server");
	}
	public void removeClient(String nameClient){
		listeClients.remove(nameClient);
		System.out.println("Au revoir");
	}
	
	
	public void sendMessage(String from, String to, String messageContent){
		messagesListe.add(new Message(from,to,messageContent));
	}
	
	
	public ArrayList<Message> getMessage(String nomClient){
		ArrayList<Message> messagesRecieved = new ArrayList<Message>();
		Iterator<Message> it = messagesListe.iterator();
		while(it.hasNext()){
			
			Message message = it.next();
			if(message.clientRecieve.equals(nomClient)){
				messagesRecieved.add(message);
			}
		}
		messagesListe.remove(messagesRecieved);
		return messagesRecieved;
	}
	
	
	static int run(ORB orb) throws Exception{
		org.omg.CORBA.Object obj;
		org.omg.PortableServer.POA rootPOA =
		org.omg.PortableServer.POAHelper.narrow(
				orb.resolve_initial_references("RootPOA"));
		
	
		org.omg.PortableServer.POAManager manager = rootPOA.the_POAManager();
	
		Dialogue_impl dialogueImpl = new Dialogue_impl();
		obj = rootPOA.servant_to_reference(dialogueImpl);
		
		Dialogue dialogue = DialogueHelper.narrow(obj);
		
		obj = orb.resolve_initial_references("NameService");
		NamingContext ctx = NamingContextHelper.narrow(obj);
	
		if (ctx==null)
		{
			System.out.println("Le composant NameService n'est pas un repertoire");
		}
		NameComponent[] name = new NameComponent[1];
		name[0] = new NameComponent("Dialogue","");
		
		ctx.rebind(name, dialogue);
		
		System.out.println("Objet cree et reference");
		manager.activate();
		orb.run();

		return 0;
	}
	
	class Message{
		public String clientSend;
		public String clientRecieve;
		public String messageContent;
		
		Message(String clientSend,String clientRecieve,String message){
			this.clientSend = clientSend;
			this.clientRecieve = clientRecieve;
			this.messageContent = message;
		}
	}
}
