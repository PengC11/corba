package chat2;


import org.omg.CORBA.ORB;
import org.omg.CORBA.Object;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

import dialogue.Dialogue;
import dialogue.DialogueHelper;


public class Client {

	public static void main(String[] args) {
		java.util.Properties props = System.getProperties();
	    
	    int status = 0;
	    ORB orb = null;

		try{
		    orb = ORB.init(args, props);
		    System.out.println("ORB initialisé");
		    run(orb);
	    }
		catch(Exception ex){
		    ex.printStackTrace();
		    status = 1;
		}
		
		if(orb != null){
		    try{
		    	orb.destroy();
		    }
		    catch(Exception ex){
		    	ex.printStackTrace();
		    	status = 1;
		    }
		}
		System.exit(status);
	}
	
	static void run(ORB orb){
		Object obj = null;
		try{
			obj=orb.resolve_initial_references("NameService");
		}
		catch(InvalidName e){
			e.printStackTrace();
			System.exit(1);
		}
		
		NamingContextExt ctx = NamingContextExtHelper.narrow(obj);
		
		if (ctx==null){
			System.out.println("Le composant NameService n'est pas un repertoire");
			System.exit(1);
		}
		NameComponent[] name = new NameComponent[1];
		name[0]=new NameComponent("Dialogue","");
		
		try{
			obj = ctx.resolve(name);
		}
		catch (Exception e){
			System.out.println("Composant inconnu");
			e.printStackTrace();
			System.exit(1);
		}
		
		Dialogue dialogue = DialogueHelper.narrow(obj);
		
		dialogue.connect("Toto");
		dialogue.connect("Titi");
		String [] clients = dialogue.getClient();
		for(int i = 0; i < clients.length; i++){
			System.out.println(clients[i]);
		}
		if(clients!=null){
			dialogue.sendMessage("Toto", "Titi", "bonjour");
			dialogue.sendMessage("Toto", "Titi", "ça va?");
			dialogue.sendMessage("Toto", "Titi", "au revoir");
		}
		String [] message = dialogue.getMessage("Titi");
		
		for(int i=0;i<message.length;i++){
			System.out.println(message[i]);
		}
	}
}
