package test;

import org.junit.Before;
import org.junit.Test;

import chat2.Dialogue_impl;

public class TestDialogue {

	private static Dialogue_impl dialogue = new Dialogue_impl(); 
	
	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testConnect() {
		dialogue.connect("Titi");
	}

	@Test
	public void testDisconnect() {
		dialogue.disconnect("Titi");
	}

	@Test
	public void testGetClient() {
		dialogue.getClient();
	}

	@Test
	public void testSendMessage() {
		dialogue.sendMessage("Titi", "Toto", "bonjours");
	}

	@Test
	public void testGetMessage() {
		dialogue.getMessage("Toto");
	}

}
