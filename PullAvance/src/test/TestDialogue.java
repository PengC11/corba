package test;

import org.junit.Before;
import org.junit.Test;

import chat3.Dialogue_impl;

public class TestDialogue {

	private static Dialogue_impl dialogue = new Dialogue_impl(); 
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetClient() {
		dialogue.getClient();
	}

	@Test
	public void testSendMessage() {
		dialogue.sendMessage("Titi", "Salut");
	}

	@Test
	public void testGetMessage() {
		dialogue.getMessage("Titi");
	}

}
