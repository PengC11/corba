package test;

import org.junit.Before;
import org.junit.Test;

import chat3.Connection_impl;

public class TestConnection {

	private static Connection_impl connection = new Connection_impl(); 
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testConnect() {
		connection.connect("Titi");
	}

	@Test
	public void testDisconnect() {
		connection.disconnect("Titi");
	}

}
