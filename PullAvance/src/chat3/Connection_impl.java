package chat3;


import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import connection.ConnectionPOA;
import dialogue.Dialogue;
import dialogue.DialogueHelper;

public class Connection_impl extends ConnectionPOA{

	@Override
	public Dialogue connect(String pseudo) {
		Server.getServer().addClient(pseudo);
		System.out.println(pseudo+" connecté");
		
		Dialogue_impl dialogueImpl = new Dialogue_impl();
		Dialogue dialogue = null;
		try{
			dialogue = DialogueHelper.narrow(_default_POA().servant_to_reference(dialogueImpl));
		}catch(WrongPolicy |ServantNotActive e){
			e.printStackTrace();
		}
		return dialogue;
	}

	@Override
	public void disconnect(String pseudo) {
		Server.getServer().removeClient(pseudo);
		System.out.println(pseudo+" déconnecté");
	}	
}
