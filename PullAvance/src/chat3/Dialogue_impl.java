package chat3;

import chat3.Server.Message;
import dialogue.DialoguePOA;

public class Dialogue_impl extends DialoguePOA{

	
	@Override
	public String[] getClient() {
		return Server.getServer().getlistClients();
	}

	@Override
	public void sendMessage(String to, String message) {
		Server.getServer().sendMessage(to, message);
	}

	@Override
	public String[] getMessage(String pseudo) {
		int nbMessage = Server.getServer().getMessage(pseudo).size();
		String [] message=new String [nbMessage];
		int i=0;
		for(Message m:Server.getServer().getMessage(pseudo)){
			message[i++]=m.messageContent;
		}
		return message;
	}

	@Override
	public boolean _is_a(String repositoryIdentifier) {
		
		return false;
	}


}
