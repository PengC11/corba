package chat3;


import org.omg.CORBA.ORB;
import org.omg.CORBA.Object;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

import connection.Connection;
import connection.ConnectionHelper;
import dialogue.Dialogue;


public class Client {
	
	public static void main(String[] args) {
		java.util.Properties props = System.getProperties();
	    
	    int status = 0;
	    ORB orb = null;

		try{
		    orb = ORB.init(args, props);
		    System.out.println("ORB initialisé");
		    run(orb);
	    }
		catch(Exception ex){
		    ex.printStackTrace();
		    status = 1;
		}
		
		if(orb != null){
		    try{
		    	orb.destroy();
		    }
		    catch(Exception ex){
		    	ex.printStackTrace();
		    	status = 1;
		    }
		}
		System.exit(status);
	}
	
	static void run(ORB orb){
		Object obj = null;
		try{
			obj=orb.resolve_initial_references("NameService");
		}
		catch(InvalidName e){
			e.printStackTrace();
			System.exit(1);
		}
		
		NamingContextExt ctx = NamingContextExtHelper.narrow(obj);
		
		if (ctx==null){
			System.out.println("Le composant NameService n'est pas un repertoire");
			System.exit(1);
		}
		NameComponent[] name = new NameComponent[1];
		name[0]=new NameComponent("Connection","");
		
		try{
			obj = ctx.resolve(name);
		}catch (Exception e){
		
			System.out.println("Composant inconnu");
			e.printStackTrace();
			System.exit(1);
		}
		Connection connection = ConnectionHelper.narrow(obj);
		Dialogue dialogue = connection.connect("Toto");
		
		for(int i = 0; i < dialogue.getClient().length; i++){
			System.out.println(dialogue.getClient()[i]);
		}
		
		dialogue.sendMessage("Titi", "bonjour");
		dialogue.sendMessage("Titi", "ça va?");
		dialogue.sendMessage("Titi", "au revoir");
		
		String [] message = dialogue.getMessage("Titi");
		for(int j=0;j<message.length;j++){
			System.out.println(message[j]);
		}
		connection.disconnect("Toto");
	}
}
