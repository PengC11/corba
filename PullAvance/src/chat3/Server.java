package chat3;

import java.util.ArrayList;
import java.util.Iterator;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;

import connection.Connection;
import connection.ConnectionHelper;

public class Server {
	ArrayList<String> listeClients = new ArrayList<String>();
	ArrayList<Message> messagesListe = new ArrayList<Message>();
	static Server server;
	ORB orb;

	public static void main(String[] args) {
		java.util.Properties props = System.getProperties();
		int status = 0;
		ORB orb = null;
		try
		{
			orb = ORB.init(args, props);
			Server.getServer().orb = orb;
			run(orb);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			status = 1;
		}
		if(orb != null)
		{
			try
			{
				orb.destroy();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				status = 1;
			}
		}
		System.exit(status);
	}
	static Server getServer(){
		if(server==null){
			server = new Server();
		}
		return server;
	}
	
	public String[] getlistClients(){
		int size = listeClients.size();
		return (String[])listeClients.toArray(new String[size]);
	}
	public void addClient(String nameClient){
		listeClients.add(nameClient);
		System.out.println("bienvenue dans le server");
	}
	public void removeClient(String nameClient){
		listeClients.remove(nameClient);
		System.out.println("Au revoir");
	}
	
	
	public void sendMessage(String to, String messageContent){
		messagesListe.add(new Message(to,messageContent));
	}
	
	
	public ArrayList<Message> getMessage(String nomClient){
		ArrayList<Message> messagesRecieved = new ArrayList<Message>();
		Iterator<Message> it = messagesListe.iterator();
		while(it.hasNext()){
			
			Message message = it.next();
			if(message.clientRecieve.equals(nomClient)){
				messagesRecieved.add(message);
			}
		}
		messagesListe.remove(messagesRecieved);//不能一边删一边加，会出现错误
		return messagesRecieved;
	}
	

	static int run(ORB orb) throws Exception{
		org.omg.CORBA.Object obj;
		org.omg.PortableServer.POA rootPOA =
		org.omg.PortableServer.POAHelper.narrow(
				orb.resolve_initial_references("RootPOA"));
		
	
		org.omg.PortableServer.POAManager manager = rootPOA.the_POAManager();
	
		NameComponent[] name = new NameComponent[1];
		
		name[0] = new NameComponent("Connection","");
		Connection_impl connectionImpl = new Connection_impl();
		obj = rootPOA.servant_to_reference(connectionImpl);
		Connection connection = ConnectionHelper.narrow(obj);
		obj = orb.resolve_initial_references("NameService");
		NamingContext ctx = NamingContextHelper.narrow(obj);
		ctx.rebind(name, connection);
		
		System.out.println("objet connection cree et reference");
		manager.activate();
		orb.run();

		return 0;
	}
	
	class Message{
		public String clientRecieve;
		public String messageContent;
		
		Message(String clientRecieve,String message){
			this.clientRecieve = clientRecieve;
			this.messageContent = message;
		}
	}
}
